<?php namespace Decoupled\Page;

use Decoupled\Core\Bundle\BaseBundle;
use Decoupled\Core\Routing\Router;
use Decoupled\Core\Application\Application;

class PageBundle extends BaseBundle{

    const NAME = 'Page';

    public function getName()
    {
        return self::NAME;
    }

    public function getDir()
    {
        return [
            'Decoupled\\Page' => dirname(__FILE__)
        ];
    }

    public function getViewDir()
    {
        return [
            'd.page' => dirname(__FILE__).'/views'
        ];
    }

    public function setRoutes( Router $state )
    {
        $state('page.common')
            ->when('$app')
            ->uses(function( $scope ){

                    $scope['baseTemplate'] = apply_filters( 'page.base.template', '@d.page/base.html.twig' );
            });


        $state('page.single')
            ->when( 'page' )
            ->uses('Decoupled\\Page\\Controller\\Page@view');          
    }
}