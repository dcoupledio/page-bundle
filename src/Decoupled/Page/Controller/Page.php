<?php namespace Decoupled\Page\Controller;

class Page{  

    public function view( $scope, $res, $post )
    {
        $scope['page'] = $post;

        return $res( '@d.page/view.html.twig', $scope );
    }
}